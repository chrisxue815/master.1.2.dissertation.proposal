\title[Style Transfer for Interactive 3D Environments]%
      {Style Transfer for Interactive 3D Environments}

\author[Jianghan Xue]
       {Jianghan Xue$^{1}$
        \\
        $^1$Trinity College Dublin, Ireland
       }

\begin{document}

\maketitle

%-------------------------------------------------------------------------
\begin{abstract}

TODO

\end{abstract}

%-------------------------------------------------------------------------
\section{Introduction}

\noindent
\textbf{Non-Photorealistic Rendering (NPR):}
After over 30 years of development on computer graphics, many problems of generating photorealistic images by computer have been solved. Even very complex scenes with many objects can be generated. In the early 1990s, some researchers turned to a new type of quest. In contrast to traditional computer graphics, they focus on creating digital art with a wide variety of artist styles, such as painting, cartoon, and technical illustration. This new area of endeavor has become known as \emph{non-photorealistic rendering}, or NPR for short.\cite{strothotte_non-photorealistic_2002}

Many NPR techniques now exist and have been implemented in some popular games (e.g. The Legend of Zelda, XIII, Team Fortress 2, Prince of Persia 4), and movies (e.g. Tarzan). It's also been used for technical illustration, medical analysis, etc.

\noindent
\textbf{Motivation:}
With so many NPR techniques available for various works at stylisation, we begin to think about how to apply them in real-time, without too much manual authoring work developing specific assets, in an efficient and optimised way.

A style transfer program takes the style of an image or a painting and render other content in this style. TODO

%-------------------------------------------------------------------------
\section{Related Work}

\noindent
\textbf{Style:}
In the visual arts, the style of a painting is a "distinctive manner which permits the grouping of works into related categories."\cite{fernie_art_1995}. Some categories are explicit:
\begin{itemize}
\item Implement: brush, pencil, sponge, knife, airbrush, etc. (and their size and shape)
\item Medium: oil, watercolour, pastel, ink (and their colour)
\item Surface: canvas, paper, wall
\item Object: figure, portrait, landscape, still life
\item Specified purpose: poster, cartoon/comic/manga, illustration
\end{itemize}

Some catogories are more subjective and difficult to describe: realism, impressionism, abstract expressionism, surrealism, photorealism, etc.

\noindent
\textbf{Style extraction:}
The first step of style transfer is style extraction.

Reinhard et al.\cite{reinhard_color_2001} introduced a global colour transfer method. They use a simple statistics analysis method to impose one image's colour characteristics on another. The images are firstly converted from RPG colour space to l$\alpha\beta$ colour space. After then, they compute the images' mean and standard deviation, and use them to globally shift the target image's colour.

Nguyen et al.\cite{nguyen_3d_2012} took a further step. They extract multiple materials (ambient, diffuse and specular colour\cite{phong_illumination_1975}) from the source image and assign the materials to 3D objects based on their positions and shapes.

The work of Shamir et al.\cite{shamir_computer_2012} shows that "the automatic computer analysis can group artists by their artistic movements, and provide a map of similarities and influential links that is largely in agreement with the analysis of art historians". Their method is based on WND-CHARM scheme\cite{shamir_evaluation_2008}, which contains a set of 4027 features that reflects many aspects of the visual content, such as shapes, textures, edges, colours, etc.

%-------------------------------------------------------------------------
\section{Method and Workplan}



%-------------------------------------------------------------------------
\section{Expected Results}

TODO

%-------------------------------------------------------------------------
\bibliographystyle{eg-alpha}

\bibliography{style-transfer-bibliography}

\end{document}
